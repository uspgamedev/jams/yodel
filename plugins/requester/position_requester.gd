extends Node
class_name PositionRequester

func request() -> Vector2:
	push_error("Virtual method")
	return Vector2()
