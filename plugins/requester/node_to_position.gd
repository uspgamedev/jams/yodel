extends PositionRequester
class_name NodeToPosition

@export var node: Node2D

func request() -> Vector2:
	return node.global_position
