extends PositionRequester
class_name GroupToPosition

@export var group_name: String

func request() -> Vector2:	
	var node = get_tree().get_first_node_in_group(group_name)
	return node.global_position
