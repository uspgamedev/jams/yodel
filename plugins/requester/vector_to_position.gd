extends PositionRequester
class_name VectorToPosition

@export var position: Vector2

func request() -> Vector2:
	return position
