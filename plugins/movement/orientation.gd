extends Node
class_name MovimentOrientation

@export_range(0., 90.) var angle := 15.

var orientation = ""

@onready var entity = get_parent()

signal orientation_changed(orientation)

func _process(_delta):
	var velocity = Vector2()
	
	if entity is CharacterBody2D:
		velocity = entity.velocity
	elif entity is RigidBody2D:
		velocity = entity.linear_velocity
	
	if velocity.is_zero_approx():
		return
	
	var prev_orientation = orientation
	if velocity.y > velocity.length()*sin(deg_to_rad(angle)):
		orientation = "front"
	elif velocity.y < -velocity.length()*sin(deg_to_rad(angle)):
		orientation = "back"
	
	
	if prev_orientation != orientation:
		orientation_changed.emit(orientation)
	

