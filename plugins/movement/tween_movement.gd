extends MovementStrategy
class_name TweenMoviment

@export var position_requester: PositionRequester
@export var duration := 1.0

signal stopped()

func start_movement():
	if is_instance_valid(entity):
		var initial_position = entity.global_position
		await get_tree().create_tween().tween_method(
			func(x):
				if is_instance_valid(entity):
					set_velocity((position_requester.request() - entity.global_position).normalized()*0.1)
					entity.global_position = (1-x)*initial_position + x*position_requester.request()
		, 0., 1., duration).finished
		
		if is_instance_valid(entity):
			set_velocity(Vector2())
	stopped.emit()
