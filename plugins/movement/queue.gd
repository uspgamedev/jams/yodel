extends Node
class_name Queue

@onready var entity = get_parent()
@onready var queue = [entity]

@export var icon: Texture2D

@export var n := 100
@export var width := 30.
@export var height := 15.

@export var scn: PackedScene

@export var k := 1.
@export var k2 := 0.01 
@export var k3 := 0.01 

@export var update: bool = false

var parent_queue = [0, 0]
var pos = [Vector2(1, -1), Vector2(1, 1	)]
var h = 1

var nodes = []

func _ready():
	if not update:
		return 
	for i in n:
		var node = scn.instantiate()
		add_child(node)
		nodes.push_back(node)
	for node in nodes:
		node.position = get_pos()
		push_back(node)
	
	update2()
	get_tree().create_timer(1.).timeout.connect(update2)

	

func update2(_x = null):
	queue = [entity]
	parent_queue = [0, 0]
	pos = [Vector2(1, -1), Vector2(1, 1)]
	h = 1

	for node in nodes:
		node.position = get_pos()
		push_back(node)
	
	if is_inside_tree():
		get_tree().create_timer(1.).timeout.connect(update2)
		

func get_tail():
	return queue[parent_queue.front()]

func get_pos() -> Vector2:
	return transform_pos(pos.front())*Vector2(width, height)
	
func push_back(node):
	parent_queue.pop_front()
	parent_queue.push_back(queue.size())
	
	var ant_pos = pos.front()
	if queue.size() == int(h*(h+1)/2.):
		parent_queue.push_back(queue.size())
		pos.push_back(ant_pos + Vector2(1, -1))
	elif queue.size() == int(h*(h+1)/2.) + 1:
		h += 1
		
	pos.push_back(ant_pos + Vector2(1, 1))
	pos.pop_front()
	
	queue.push_back(node)
	
func push_front(node):
	queue.pop_front()
	queue.push_front(node)
	queue.push_front(entity)

func transform_pos(pos2: Vector2) -> Vector2:
	#var aux = Vector2(pos.x, pos.y*k*log(k2*pos.x))
	#var aux = Vector2(pos.x, pos.y*k/log(k2*pos.x))
	#var aux = Vector2(pos.x, pos.y*k/log(k2*h))
	#var aux = Vector2(pos.x, pos.y*(k/pow(h, k3)))
	
	var aux = Vector2(pos2.x, pos2.y/pow((h+k2), k)) #*(1. - 1./pow(k2*(h+1), k3)))
	#print(pos2, " ", h, " ", aux)
	
	#if aux.y < 0.:
		#aux.y = log(-aux.y)
	
	
	return aux

