extends MovementStrategy
class_name ChaseMoviment

@export var position_requester: PositionRequester
@export var speed := 20.0
@export var distance_limit := 0.0

func chase():
	var distance = (position_requester.request() - entity.global_position)
	var velocity = Vector2()
	if distance.length() > distance_limit:
		velocity = distance.normalized()*speed
	set_velocity(velocity)
 		
func _physics_process(_delta):
	if is_instance_valid(entity):
		chase()
		move()
