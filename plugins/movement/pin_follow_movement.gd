extends MovementStrategy
class_name PinFollowMoviment

@onready var pin: Joint2D

@export var to_follow: RigidBody2D
@export var distance := Vector2(150.0, 0.)
@export var min_offset := Vector2(0.0, -10.0)
@export var max_offset := Vector2(15.0, 10.0)

var offset2 := Vector2()

var offset := Vector2()
var v := Vector2()
var av := Vector2()
var w := 0.
var aw := 0.



var ang := 0.1
var dis := Vector2()

var l = 0

func start_movement():
	if not is_instance_valid(to_follow):
		return
		
	randomize()
	distance.x += randf_range(-10, 10)
		
	set_velocity(Vector2(-0.01, 0.))
	entity.angular_velocity = 0.
	if entity is RigidBody2D:
		offset = distance + offset2
				#+ Vector2(randf_range(min_offset.x, max_offset.x), randf_range(min_offset.y, max_offset.y))
		entity.position = to_follow.position + offset
 		
func _process(delta):
	if is_instance_valid(entity):
		if entity is RigidBody2D:
			aw = -10.*ang
			w += aw*delta
			if (offset + dis).length() > offset.length() + 20.:
				av = -Vector2(randf_range(5, 100), randf_range(5, 100))*Vector2(exp(dis.x/100.0), exp(dis.y/100.0))
				av.y = 0.
				v += av*delta
			else:
				v = Vector2()
				av = Vector2()
			
			ang += w*delta
			dis += v*delta
			
			#entity.rotation = ang
			entity.global_position = to_follow.global_position + offset	 + dis
