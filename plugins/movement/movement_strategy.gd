extends Node
class_name MovementStrategy

@onready var entity = (get_parent() if get_parent() is CharacterBody2D or get_parent() is RigidBody2D else null)

func set_velocity(velocity):
	if entity is CharacterBody2D:
		entity.velocity = velocity
	elif entity is RigidBody2D:
		entity.linear_velocity = velocity

func move():
	if entity is CharacterBody2D:
		entity.move_and_slide()
	elif entity is RigidBody2D:
		pass
	
