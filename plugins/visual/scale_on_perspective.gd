@tool
extends Node
class_name PerspectiveScale

@onready var entity: Node2D = get_parent() if get_parent() is Node2D else null

@export var levels: Array[float] = []
@export var sizes: Array[float] = []

@export var offset := -540

@export var active := true

func _process(_delta):
	if active:
		rescale()
		
func rescale():
	var y = entity.global_position.y
	var i = 0
	
	if !Engine.is_editor_hint():
		levels.sort()
	
	for level in levels:
		if y > level:
			i += 1

	if i == 0:
		return
	
	i-=1
	
	if i >= levels.size()-1:
		entity.scale = Vector2.ONE * sizes[levels.size()-1]
		return
		
	entity.scale = Vector2.ONE * (sizes[i] + (y - levels[i])*(sizes[i+1] - sizes[i])/(levels[i+1] - levels[i]))
	entity.offset.y = (sizes[i] + (y - levels[i])*(sizes[i+1] - sizes[i])/(levels[i+1] - levels[i]))*offset
