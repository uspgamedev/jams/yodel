extends Node
class_name MovementDirection

@onready var entity = get_parent()
@export var sprite: Node2D

func _process(_delta):
	if entity is CharacterBody2D:
		if not is_zero_approx(entity.velocity.x):
			sprite.flip_h =  entity.velocity.x > 0.
	elif entity is RigidBody2D:
		if not is_zero_approx(entity.linear_velocity.x):
			sprite.flip_h =  entity.linear_velocity.x > 0.
