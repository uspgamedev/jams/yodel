class_name SignalBusSender
extends Node


func send_card_drawn(card: CardSpec, to_position: int, cards_left: int):
	SignalBus.send(&'card_drawn', [card, to_position, cards_left])


func send_card_played(card: CardSpec, from_position: int, total_played: int):
	SignalBus.send(&'card_played', [card, from_position, total_played])


func send_card_returned(card: CardSpec, from_position: int, total_played: int):
	SignalBus.send(&'card_returned', [card, from_position, total_played])


func send_play_confirmed(cards: Array[CardSpec], total_discarded: int):
	SignalBus.send(&'play_confirmed', [cards, total_discarded])


func send_piles_reshuffled(total_shuffled: int):
	SignalBus.send(&'piles_reshuffled', [total_shuffled])


func send_score_changed(old_value: int, new_value: int):
	SignalBus.send(&'score_changed', [old_value, new_value])


func send_tier_reached(tier: ProgressTier):
	SignalBus.send(&'tier_reached', [tier])


func send_journey_ended(score: int):
	SignalBus.send(&'journey_ended', [score])
	
