extends Node


@export var main_menu_scene: PackedScene
@export var options_scene: PackedScene
@export var score_scene: PackedScene
@export var level_scene: PackedScene
@export var how_to_play: PackedScene
@export var credits_scene: PackedScene

var current_scene_root: Node 


func go_to_main_menu():
	_change_to_scene(main_menu_scene.instantiate())


func go_to_options():
	_change_to_scene(options_scene.instantiate())


func go_to_tutorial():
	_change_to_scene(how_to_play.instantiate())
	

func go_to_level():
	_change_to_scene(level_scene.instantiate())


func go_to_score(score: int):
	var score_inst = score_scene.instantiate()
	score_inst.score = score
	_change_to_scene(score_inst)


func go_to_credits():
	_change_to_scene(credits_scene.instantiate())

	
func _change_to_scene(node: Node):
	current_scene_root.queue_free()
	await current_scene_root.tree_exited
	current_scene_root = node
	get_tree().root.add_child(node)
