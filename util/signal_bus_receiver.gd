class_name SignalBusReceiver
extends Node

signal card_drawn(card: CardSpec, to_position: int, cards_left: int)
signal card_played(card: CardSpec, from_position: int, total_played: int)
signal card_returned(card: CardSpec, from_position: int)
signal play_confirmed(cards: Array[CardSpec], total_discarded: int)
signal piles_reshuffled(total_shuffled: int)
signal score_changed(old_value: int, new_value: int)
signal tier_reached(tier: ProgressTier)
signal journey_ended(score: int)

const GROUP := &'RECEIVER_GROUP'

static var _s_dirty := true
static var _s_receivers: Array[SignalBusReceiver] = []


func _enter_tree():
	add_to_group(GROUP)
	_s_dirty = true


func _exit_tree():
	_s_dirty = true


static func all(tree: SceneTree) -> Array[SignalBusReceiver]:
	if _s_dirty:
		_s_receivers.clear()
		for receiver: SignalBusReceiver in tree.get_nodes_in_group(GROUP):
			if receiver:
				_s_receivers.append(receiver)
		_s_dirty = false
	return _s_receivers
