extends Camera2D


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	var half_screen = get_viewport_rect().size / 2
	
	position.x = half_screen.x + get_viewport().get_mouse_position().x / 30
	position.y = half_screen.y + get_viewport().get_mouse_position().y / 8
