extends Control


@onready var music_bus_id = AudioServer.get_bus_index("Music")
@onready var sfx_bus_id = AudioServer.get_bus_index("SFX")
@onready var card_sounds_bus_id = AudioServer.get_bus_index("CardSounds")


func _ready():
	self.visible = false


#func _input(event):
	#if (
		#event.is_action_pressed("pause") 
		#and !self.visible
	#):
			#self.visible = true
			#get_tree().paused = true
			


func _on_options_button_pressed():
		self.visible = true
		get_tree().paused = true


func _on_back_pressed():
	self.visible = false
	get_tree().paused = false
