class_name OptionsMenu
extends Control


@onready var music_bus_id = AudioServer.get_bus_index("Music")
@onready var sfx_bus_id = AudioServer.get_bus_index("SFX")
@onready var card_sounds_bus_id = AudioServer.get_bus_index("CardSounds")


func _ready():
	self.visible = false


#func _input(event):
	#if (
		#event.is_action_pressed("pause") 
		#and !self.visible
	#):
			#self.visible = true
			#get_tree().paused = true
			

func _on_music_slider_value_changed(value):
	AudioServer.set_bus_volume_db(music_bus_id, linear_to_db(value))
	AudioServer.set_bus_mute(music_bus_id, value < 0.05)


func _on_sfx_slider_value_changed(value):
	AudioServer.set_bus_volume_db(sfx_bus_id, linear_to_db(value))
	AudioServer.set_bus_mute(sfx_bus_id, value < 0.05)


func _on_card_sounds_slider_value_changed(value):
	AudioServer.set_bus_volume_db(card_sounds_bus_id, linear_to_db(value))
	AudioServer.set_bus_mute(card_sounds_bus_id, value < 0.05)


func _on_options_button_pressed():
		self.visible = true
		get_tree().paused = true


func _on_quit_pressed():
	get_tree().quit()


func _on_back_pressed():
	self.visible = false
	get_tree().paused = false


func _on_menu_pressed():
	SceneSwitcher.go_to_main_menu()
