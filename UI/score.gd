extends Control


var score: int


func _ready():
	show_score()
	

func show_score():
	%ScoreLabel.text = "%05d" % score
	if score < 150: %BadEnding.show()
	else: %GoodEnding.show()
	self.visible = true


func _on_main_menu_pressed():
	SceneSwitcher.go_to_main_menu()


func _on_quit_pressed():
	get_tree().quit()
