extends Control


func _ready():
	SceneSwitcher.current_scene_root = self


func _on_play_pressed():
	SceneSwitcher.go_to_level()


func _on_options_pressed():
	SceneSwitcher.go_to_options()


func _on_tutorial_pressed():
	SceneSwitcher.go_to_tutorial()
	
	
func _on_quit_pressed():
	get_tree().quit()


func _on_credits_pressed():
	SceneSwitcher.go_to_credits()
