class_name CreatureSpec
extends Resource

@export var sprites : SpriteFrames
@export var musical_taste : String
@export var musical_distaste : String
@export var score := 0
@export var score_to_unlock := 0
@export var type := Type.SHEEP

enum Type {
	SHEEP,
	WOLF
}
