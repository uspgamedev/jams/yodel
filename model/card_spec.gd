class_name CardSpec
extends Resource

@export var art_texture: Texture2D
@export var musical_phrase: AudioStream
@export var symbol: String
