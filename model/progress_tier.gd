class_name ProgressTier
extends Resource

@export var threshold := 0
@export var visible_sheep_min := 4
@export var visible_sheep_max := 6
@export var visible_props_min := 4
@export var visible_props_max := 7
@export var introduced_cards: Array[CardSpec]
@export var introduced_sheeps: Array[CreatureSpec]
@export var scenario_props: Array[Texture2D]
