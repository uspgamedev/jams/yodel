extends MarginContainer

@export_category('Node references')
@export var draw_pile: CardPile

@export_category('Settings')
@export var card_slot_scn: PackedScene
@export var card_copies := 2
@export var delay_before := 3.0
@export var delay_between := 0.1

@onready var _cards := %Cards

var _lock: int
var first_new_tunes = true

func _ready():
	hide()


func _on_tier_reached(tier: ProgressTier):
	_lock = SignalBus.lock()
	if first_new_tunes:
		await get_tree().create_timer(2).timeout
		first_new_tunes = false
	show()
	for card: CardSpec in tier.introduced_cards:
		for _i in card_copies:
			var slot := card_slot_scn.instantiate()
			slot.card_view.card = card
			_cards.add_child(slot)


func _hide_if_empty():
	if _cards.get_child_count() == 0:
		SignalBus.unlock(_lock)
		hide()


func _move_to_pile(slot: CardSlot, card_pile: CardPile):
	%FlapSFX.play()
	await slot.card_view.tween_to(card_pile.get_card_transform())
	slot.queue_free()


func _on_continue_pressed():
	var count := 0
	#%Continue.hide()
	for slot: CardSlot in _cards.get_children():
		count += 1
		slot.tree_exited.connect(self._hide_if_empty, CONNECT_ONE_SHOT)
		get_tree().create_timer(count * delay_between).timeout.connect(
			self._move_to_pile.bind(slot, draw_pile)
		)
