extends TextureButton


func _ready():
	hide()


func _on_card_played(_card, _from_position, _total_played):
	show()
	

func _on_card_returned(_card, _from_position, total_played):
	if total_played <= 0:
		hide()


func _on_play_confirmed(_cards, _total_discarded):
	hide()
