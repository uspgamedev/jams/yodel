extends PanelContainer

var score := 0:
	set(value):
		score = value
		if _score_value:
			_score_value.text = "%05d" % value

var _tween: Tween

@onready var _score_value: Label = %ScoreValue


func _on_score_changed(_old_value: int, new_value: int):
	if _tween:
		_tween.kill()
	_tween = create_tween()
	_tween.tween_property(self, 'score', new_value, 0.2)
