extends Node2D

@export var hand: Hand
@export var card_view_scn: PackedScene
@export var radius: float
@export var velocity: float

var angle: float


func _process(delta: float):
	_rotate(delta)


func _rotate(delta: float):	
	for card in _child_cards():
		if card is CardView:
			card.position = card.position.rotated(velocity * delta)


func _calculate_offset():
	angle = 0
	var cards := _child_cards()
	for card in cards:
		if card is CardView:
			card.position = Vector2(radius, 0).rotated(angle) 
			angle += 2*PI/cards.size()


func _on_children_changed(_unused):
	_calculate_offset()


func _child_cards() -> Array:
	return get_children().filter(func(node): return node is CardView)


func _on_card_played(card: CardSpec, _from_position, _total_played):
	var card_view := card_view_scn.instantiate() as CardView
	card_view.card = card
	add_child(card_view)


func _on_card_returned(_card: CardSpec, from_position: int, _total_played):
	var card_view := _child_cards()[from_position] as CardView
	if card_view:
		remove_child(_child_cards()[from_position])
	

func _on_play_confirmed(_cards, _total_discarded):
	for child in _child_cards():
		child.queue_free()


func _on_return_cards_button_pressed():
	for i in range(_child_cards().size()):
		CardPlay.locate(get_tree()).return_card(0)
