extends PanelContainer

@onready var turn_distance = %ProgressIcon.position.x / 50
var updating := false
var current_turn := 0

func _process(delta):
	if updating:
		%ProgressIcon.position.x -= turn_distance * delta

func _on_signal_bus_receiver_play_confirmed(_cards, _total_discarded):
	await get_tree().create_timer(5).timeout
	current_turn += 1
	%ProgressCounter.text = str(current_turn) + " / 50"
	updating = true
	await get_tree().create_timer(1).timeout
	updating = false
	
