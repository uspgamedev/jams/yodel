extends MarginContainer

@export var log_entry_scn: PackedScene

@onready var _contents: Control = %Contents

var _sheep_buffer: Array[Dictionary]


func _ready():
	get_tree().node_added.connect(self._on_node_added)


func toggle_contents(active: bool):
	%List.visible = active


func _on_node_added(node: Node):
	if node is Sheep:
		var taste := node.get_node('Taste') as Taste
		await taste.ready
		taste.riffs.connect(self._on_sheep_reacted.bind(taste.taste))


func _on_play_confirmed(cards: Array[CardSpec], _total_discarded):
	var logged_sheep_types: Array = []
	
	for sheep_reaction in _sheep_buffer:
		if not sheep_reaction.sheep in logged_sheep_types:
			var log_entry := log_entry_scn.instantiate() as LogEntry
			log_entry.played_cards = cards.duplicate()
			log_entry.sheep = sheep_reaction.sheep
			log_entry.tunes_good = sheep_reaction.good
			log_entry.tunes_bad = sheep_reaction.bad
			if %List.visible: %List.add_child(log_entry)
			else: flash_logs(log_entry)
			logged_sheep_types.append(sheep_reaction.sheep)
	_sheep_buffer.clear()

func flash_logs(log_entry):
	%FlashList.add_child(log_entry)
	await get_tree().create_timer(3).timeout
	%FlashList.remove_child(log_entry)
	%List.add_child(log_entry)


func _on_sheep_reacted(good: int, bad: int, sheep: CreatureSpec):
	_sheep_buffer.append({good = good, bad = bad, sheep = sheep})

