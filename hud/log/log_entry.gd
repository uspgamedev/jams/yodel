class_name LogEntry
extends PanelContainer

@export var played_cards: Array[CardSpec]
@export var sheep: CreatureSpec
@export var tunes_good := 0
@export var tunes_bad := 0

@onready var _sheep_texture: TextureRect = %Sheep
@onready var _cards: Array[CardSlot] = [
	%CardSlotSmall1 as CardSlot,
	%CardSlotSmall2 as CardSlot,
	%CardSlotSmall3 as CardSlot
]
@onready var _good_counter := %GoodCounter
@onready var _good_counter_label := %GoodCount
@onready var _bad_counter := %BadCounter
@onready var _bad_counter_label := %BadCount
@onready var _neutral_counter := %NeutralCounter

func _ready():
	await get_tree().process_frame
	_sheep_texture.texture = sheep.sprites.get_frame_texture(&'idle', 0)
	# FIXME: hardcoded max played cards
	for i in 3:
		_cards[i].card_view.card = played_cards[i]
	var total := tunes_good - tunes_bad
	if total < 0:
		_good_counter.hide()
		_neutral_counter.hide()
		_bad_counter.show()
		_bad_counter_label.text = str(-total)
	elif total > 0:
		_bad_counter.hide()
		_neutral_counter.hide()
		_good_counter.show()
		_good_counter_label.text = str(total)
	else:
		_bad_counter.hide()
		_good_counter.hide()
		_neutral_counter.show()


