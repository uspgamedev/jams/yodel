@tool
class_name Hand extends Control

@export_category('Node references')
@export var draw_pile: CardPile
@export var discard_pile: CardPile
@export var card_view_scn: PackedScene

@export_category('Display')
@export var angle_gap := 10.0
@export var ellipse_radii := Vector2(200, 400)
@export var rotation_factor := 1.0

@onready var _cards: Node2D = %Cards


func _ready():
	if not Engine.is_editor_hint():
		CardPlay.locate(get_tree()).start_turn()


func _draw():
	var card_count := _cards.get_child_count()
	var angle_total := maxf(angle_gap * (card_count - 1), 0.0)
	var angle_start := -angle_total / 2
	for i in card_count:
		var card_view := _cards.get_child(i) as CardView
		if card_view:
			var angle := angle_start + angle_gap * i
			card_view.position = _angle_to_position(angle)
			card_view.rotation = deg_to_rad(angle * rotation_factor)


func _on_card_entered(_card_view: CardView):
	queue_redraw()


func _on_card_left(_card_view: CardView):
	queue_redraw()


func _angle_to_position(angle: float) -> Vector2:
	var angle_rad := deg_to_rad(angle)
	return Vector2(
		ellipse_radii.x * sin(angle_rad), ellipse_radii.y * -cos(angle_rad)
	)


func _on_card_drawn(card: CardSpec, _to_position: int, _cards_left):
	var lock := SignalBus.lock()
	var card_view := card_view_scn.instantiate() as CardView
	card_view.card = card
	card_view.card_selected.connect(
		self._on_card_selected.bind(card_view)
	)
	_cards.add_child(card_view)
	$CardShuffle.play()
	await card_view.tween_from(draw_pile.get_card_transform())
	card_view.selectable = true
	SignalBus.unlock(lock)


func _on_card_selected(card_view: CardView):
	var from_position := _cards.get_children().find(card_view)
	if CardPlay.locate(get_tree()).play_card(from_position):
		card_view.selectable = false


func _on_card_played(_card: CardSpec, from_position: int, _total_played: int):
	var card_view := _cards.get_child(from_position)
	if card_view:
		card_view.queue_free()
		$CardPlay.play()


func _on_card_returned(card: CardSpec, _from_position: int, _total_played: int):
	var card_view := card_view_scn.instantiate()
	card_view.card = card
	card_view.selectable = true
	card_view.card_selected.connect(
		self._on_card_selected.bind(card_view)
	)
	_cards.add_child(card_view)
	$CardPlay.play()
