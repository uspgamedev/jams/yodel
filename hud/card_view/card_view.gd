@tool
class_name CardView
extends Node2D

signal card_selected(card: CardView)

@export var selectable := false
@export var contents: TextureRect
@export var card: CardSpec:
	set(new_card):
		if contents and new_card:
			contents.texture = new_card.art_texture
			%Label.text = new_card.symbol
		card = new_card
@export var card_riff: AudioStreamPlayer

@onready var _motion_xform: Node2D = %MotionTransform
@onready var _highlight_xform: Node2D = %HighlightTransform

var _highlight_tween: Tween


func tween_from(xform: Transform2D):
	await get_tree().process_frame
	_motion_xform.global_transform = xform
	var tween := create_tween().tween_property(
		_motion_xform, 'transform', Transform2D.IDENTITY, 0.2
	)
	await tween.finished


func tween_to(xform: Transform2D):
	var tween := create_tween().tween_property(
		_motion_xform, 'global_transform', xform, 0.2
	)
	await tween.finished


func _on_mouse_entered():
	if not Engine.is_editor_hint() and selectable:
		if _highlight_tween:
			_highlight_tween.kill()
		_highlight_tween = create_tween()
		_highlight_tween.tween_property(
			_highlight_xform, 'scale', Vector2.ONE * 1.2, 0.1
		)
		$CardHover.play ()


func _on_mouse_exited():
	if not Engine.is_editor_hint() and selectable:
		if _highlight_tween:
			_highlight_tween.kill()
		_highlight_tween = create_tween()
		_highlight_tween.tween_property(
			_highlight_xform, 'scale', Vector2.ONE, 0.1
		)


func _on_gui_input(event: InputEvent):
	if event is InputEventMouseButton:
		if event.button_index == 1 and event.is_released():
			card_selected.emit()
			
		if (
			event.button_index == 2 
			and card
			and event.is_released()
		):
			$CardRiff.stream = card.musical_phrase
			$CardRiff.play()
			
			for card in get_tree().get_nodes_in_group("card_view"):
				if card != self: card.card_riff.stop()
