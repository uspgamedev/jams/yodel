class_name ConfirmPlayButton
extends Button

@onready var base_size := custom_minimum_size


func _draw():
	var scaled := 1.0
	match get_draw_mode():
		DRAW_PRESSED:
			scaled = 1.5
		DRAW_HOVER:
			scaled = 1.25
		DRAW_HOVER_PRESSED:
			scaled = 1.5
	custom_minimum_size = base_size * scaled


func _on_pressed():
	var cardplay := CardPlay.locate(get_tree())
	if cardplay:
		cardplay.confirm_play()
 

func _on_card_played(_card, _from_position, total_played: int):
	var cardplay := CardPlay.locate(get_tree())
	if cardplay and total_played >= cardplay.played_card_max:
		show()
	else:
		hide()


func _on_card_returned(_card, _from_position, _total_played):
	hide()
	

func _on_play_confirmed(_cards, _total_discarded):
	hide()
