class_name CardPile
extends HBoxContainer

@onready var _card_view: CardView = %CardView


func get_card_transform() -> Transform2D:
	return _card_view.global_transform
