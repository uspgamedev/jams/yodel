extends Node

@onready var counter: Label = %Label


func _on_play_confirmed(_cards, total_discarded: int):
	counter.text = str(total_discarded)


func _on_piles_reshuffled(_total_shuffled):
	counter.text = '0'
