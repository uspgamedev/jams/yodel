extends Node

@onready var counter: Label = %Label


func _on_card_drawn(_card, _to_position, cards_left: int):
	counter.text = str(cards_left)


func _on_piles_reshuffled(total_shuffled: int):
	counter.text = str(total_shuffled)
