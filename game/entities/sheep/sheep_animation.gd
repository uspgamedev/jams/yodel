extends AnimatedSprite2D
class_name CreatureAnimation

@export var current_animation = "idle"
@export var orientation = ""

func change_orientation(orientation_):
	orientation = orientation_
	play_animation()

func idle():
	current_animation = "idle"
	play_animation()

func walk():
	current_animation = "walk"
	play_animation()

func play_animation():
	if sprite_frames.has_animation(current_animation+orientation):
		play(current_animation+orientation)
