extends RigidBody2D
class_name Sheep

@export var state: SheepState
@export var taste: Taste

static func get_sheeps() -> Array:
	return Engine.get_main_loop().get_nodes_in_group("SHEEP")
