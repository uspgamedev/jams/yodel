extends Node2D
class_name CreatureSpawner

@export var min_offset := Vector2(-120., -10.)
@export var max_offset := Vector2(120., 10.)

@export var min_offset_below := Vector2(-70., -10.)
@export var max_offset_below := Vector2(70., 10.)

@export var entities_node: Node

@onready var map: MapManager = MapManager.locate() 

func spawn(creature, level, column):
	if randf_range(0., 1.) > 0.5:
		creature.global_position.x = map.columns[column] + randf()*(max_offset.x - min_offset.x)
		creature.position.y = map.levels[level][0] +\
			 min_offset.y + randf()*(max_offset.y - min_offset.y)
	else:
		creature.global_position.x = map.columns[column] + randf()*(max_offset_below.x - min_offset_below.x)
		creature.position.y = map.levels[level][1] +\
			 min_offset_below.y + randf()*(max_offset_below.y - min_offset_below.y)
	
	
	entities_node.add_child(creature)
	
	
