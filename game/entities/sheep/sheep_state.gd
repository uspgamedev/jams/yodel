extends Node
class_name SheepState

enum State {
	IDLE,
	CHASE,
	FOLLOW,
	WALK,
	WOLF_WALK,
	DEAD
}

@export var state: State = State.IDLE : set = set_state
@export var taste: Taste
@export var moviment_orientation: MovimentOrientation
@export var sprite: Node2D

signal begin_walk()
signal stopped()

var current_movement: MovementStrategy

var level := 0
var column := 0

@onready var entity = get_parent()

func set_state(state_):
	state = state_
	if is_inside_tree():	
		update_state()
				

func _ready():
	update_state()

func update_state():
	if is_instance_valid(current_movement):
		current_movement.entity = null
	match state:
		State.IDLE:
			stopped.emit()
			current_movement = %Idle
			
		State.CHASE:
			begin_walk.emit()
			current_movement = %ChasePlayer
			%ChasePlayer.entity = entity
			%ChasePlayer.duration = Shepherd.locate().state.SONG_DURATION
			%ChasePlayer.start_movement()
			
		State.FOLLOW:
			stopped.emit()
			Shepherd.locate().state.begin_walk.connect(func(): self.begin_walk.emit())
			Shepherd.locate().state.stopped.connect(func(): self.stopped.emit())
			
			current_movement = %FollowPlayer
			var head_queue: Queue = Shepherd.locate().queue
			var prev_tail = head_queue.get_tail()
			var offset = head_queue.get_pos()
			head_queue.push_back(entity)
			%FollowPlayer.entity = entity
			%FollowPlayer.to_follow = prev_tail
			%FollowPlayer.offset2 = offset
			%FollowPlayer.offset2.y *= sprite.scale.x 
			
			%FollowPlayer.start_movement()
			entity.set_collision_mask(0)
			entity.set_collision_layer(0)

			CardPlay.locate(get_tree()).earn_points(taste.taste.score)		
		State.WALK:
			begin_walk.emit()
			var lev 
			if entity.global_position.y > MapManager.locate().center_level:
				lev = MapManager.locate().levels[level][1]
			else:
				lev = MapManager.locate().levels[level][0]
			
			walk_to(Vector2(entity.global_position.x, lev))
			%ChangeLevel.stopped.connect(func(): state = State.IDLE, CONNECT_ONE_SHOT)
			%ChangeLevel.stopped.connect(func(): self.stopped.emit())
		State.WOLF_WALK:
			begin_walk.emit()
			var sheeps = Shepherd.locate().queue.queue.filter(func(sheep): return sheep is Sheep and sheep.state.state != State.DEAD)
			var tastes = sheeps.map(func(x): return x.taste.taste)
			var tastes2 = tastes.duplicate()
			tastes2.sort_custom(func(spec1: CreatureSpec, spec2: CreatureSpec): return spec1.score > spec2.score)
			var max_score_spec = tastes2.front() 
			if max_score_spec != null:
				sheeps[tastes.find(max_score_spec)].state.state = State.DEAD
				sheeps[tastes.find(max_score_spec)].visible = false
				CardPlay.locate(get_tree()).earn_points(-max_score_spec.score)
			
			var lev
			if moviment_orientation.orientation == "back":
				lev = MapManager.locate().levels[level][1]
			else:
				lev = MapManager.locate().levels[level][0]
			
			walk_to(Vector2(entity.global_position.x, lev))
			%ChangeLevel.stopped.connect(func(): self.entity.	queue_free())
		
		
	current_movement.entity = entity

func _on_taste_riffs(accepted, rejected):
	level -= accepted - rejected
	if level >= MapManager.locate().levels.size():
		var height = 250.0 if (entity.global_position.y < MapManager.locate().center_level) else float(MapManager.locate().viewport_height)
		walk_to(Vector2(entity.global_position.x, height))
		%ChangeLevel.stopped.connect(func(): self.entity.queue_free(), CONNECT_ONE_SHOT)
		
	elif level < 0:
		match state:
			State.IDLE:
				state = State.CHASE
	elif accepted - rejected != 0:
		match state:
			State.IDLE:
				state = State.WALK

func _on_chase_player_stopped():
	chase()

func _on_area_2d_body_entered(body):
	if body == Shepherd.locate():
		chase()

func chase():
	match state:
		State.CHASE:
			match taste.taste.type:
				CreatureSpec.Type.SHEEP:
					state = State.FOLLOW
					taste.done = true
					
				CreatureSpec.Type.WOLF:
					state = State.WOLF_WALK
					taste.done = true
		

func walk_to(pos):
	%LevelPosition.position = pos
	current_movement = %ChangeLevel
	%ChangeLevel.entity = entity
	%ChangeLevel.duration = Shepherd.locate().state.SONG_DURATION
	%ChangeLevel.start_movement()
		
	
