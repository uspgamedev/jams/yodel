extends CreatureSpawner

@export var background: ParallaxBackground


func spawn(creature, level, column):
	var layers = get_layers()

	
	creature.global_position.x = map.columns[column] + min_offset.x + randf()*(max_offset.x - min_offset.x) 
	creature.global_position.y = layers[level].position.y +\
		min_offset.y + randf()*(max_offset.y - min_offset.y) +\
		level * map.viewport_height/layers.size()
	
	creature.initial_position = creature.global_position
	
	entities_node.add_child(creature)
	creature.movement_scale = layers[level].motion_scale
		
			
func get_layers():
	return background.get_children().filter(func (x): return x is ParallaxLayer)
	
