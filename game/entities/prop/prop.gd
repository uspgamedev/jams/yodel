extends Sprite2D
class_name Prop


var camera_position = null
var initial_position: Vector2 
var movement_scale: Vector2

@export_range(-10., 10.) var c := 1.


func _physics_process(_delta):
	if camera_position != null and get_viewport().get_camera_2d() != null:
		update_position()
		
	if camera_position == null and get_viewport().get_camera_2d() != null:
		camera_position = get_viewport().get_camera_2d().global_position
#

func generate_prop(prop_list: Array[Texture2D]):	
	var prop: Texture2D = prop_list.pick_random()
	texture = prop
	offset.y = -prop.get_height()

func update_position():
	global_position.x = initial_position.x + (get_viewport().get_camera_2d().global_position.x - camera_position.x) * ( 1.- movement_scale.x)
