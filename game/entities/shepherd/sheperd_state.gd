extends Node
class_name ShepherdState

enum State {
	STOP,
	WALK
}

signal stopped()
signal begin_walk()
signal begin_song()

@export var state: State = State.WALK : set = set_state
@export var animated_sprite: AnimatedSprite2D
@export var SONG_DURATION := 5.

@onready var current_movement: MovementStrategy = get_child(0)

@onready var entity = get_parent()

func set_state(state_):
	state = state_
	if is_inside_tree():	
		update_state()


func _ready():
	update_state()
	%Walk.stopped.connect(stop_walking)


func update_state():
	current_movement.entity = null
	match state:
		State.WALK:
			begin_walk.emit()
			current_movement = %Walk
			%NextStop.node = entity.first_stop
			current_movement.entity = entity
			current_movement.start_movement()
			animated_sprite.play("walking")
		State.STOP:
			stopped.emit()
			%Camera2D.enabled = true
			current_movement = %Idle
			CardPlay.locate(get_tree()).start_turn()
			animated_sprite.play("idle")
	current_movement.entity = entity

func sing(_x, _y):
	animated_sprite.play("summon")
	begin_song.emit()
	await get_tree().create_timer(SONG_DURATION).timeout
	state = State.WALK

func stop_walking():
	%NextStop.node.position.x -= MapManager.locate().viewport_width/3.
	state = State.STOP
