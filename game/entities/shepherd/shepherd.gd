extends Node2D
class_name Shepherd

@export var first_stop: Node2D
@export var queue: Queue
@export var state: ShepherdState
@export var stage_manager: Node

static func locate() -> Shepherd:
	return Engine.get_main_loop().get_first_node_in_group("SHEPHERD")


func _on_sprite_2d_animation_finished():
	$Sprite2D.play("play")
