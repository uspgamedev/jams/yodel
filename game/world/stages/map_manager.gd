extends Node
class_name MapManager

const GROUP = "MAP_MANAGER"

var num_levels = 3

@export var levels: Array = [[530, 720], 
						   	[400, 840],
					   		[370, 950]]
@export var center_level := 650
@export var viewport_width := 1920 
@export var viewport_height := 1080 
@onready var columns = [viewport_width/6., viewport_width/3. + viewport_width/6., 2*viewport_width/3. + viewport_width/6.]

func _ready():
	add_to_group(GROUP)

func advance():
	columns = columns.map(func(x): return x - viewport_width/3.)

static func locate() -> MapManager:
	return Engine.get_main_loop().get_first_node_in_group(GROUP)
