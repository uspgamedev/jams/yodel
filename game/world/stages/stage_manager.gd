extends Node


var current_tier: ProgressTier
@export var sheep_scene: PackedScene
@export var prop_scene: PackedScene
@export var background: ParallaxBackground

@export var card_play: CardPlay
@export var sheep_spawner: CreatureSpawner
@export var prop_spawner: CreatureSpawner
@export var map: MapManager

@export var cycles_to_end: int

var current_cycle = 0

func _ready():
	for i in map.columns.size():
		spawn_sheeps.bind(i).call_deferred()
		spawn_props.bind(i).call_deferred()
	Shepherd.locate().state.begin_song.connect(advance_map)
	Shepherd.locate().state.stopped.connect(kill_sheeps)


func advance_map():
	map.advance()
	spawn_sheeps()
	spawn_props.call_deferred()

func spawn_sheeps(column := 0):
	current_tier = card_play.get_current_tier()
	for _i in int(randi_range(current_tier.visible_sheep_min, current_tier.visible_sheep_max)/3.):
		spawn_sheep_on_column.bind(column).call_deferred()


func spawn_props(column := 0):
	var layers = background.get_children().filter(func (x): return x is ParallaxLayer)
	var levels = range(2, int(layers.size()/2.)+1)
	levels.shuffle()
	for _i in int(randi_range(current_tier.visible_props_min, current_tier.visible_props_max)/3.):
		if levels.is_empty():
			break
			
		var prop = prop_scene.instantiate()
		prop.generate_prop(current_tier.scenario_props)
		prop_spawner.spawn(prop, levels.pop_back(), column)


func spawn_sheep_on_column(column := 0):
	var sheep = sheep_scene.instantiate()
	var level = randi_range(0, map.levels.size()-1)
	sheep_spawner.spawn(sheep, level, column)
	
	var sheep_state: SheepState = sheep.state
	sheep_state.level = level
	sheep_state.column = column
	
	
func kill_sheeps():
	for sheep in Sheep.get_sheeps():
		if sheep.state.state == SheepState.State.IDLE and sheep.global_position.x >= map.columns.back()+map.viewport_width/6.:
			sheep.queue_free()

func _on_signal_bus_receiver_journey_ended(score):
	SceneSwitcher.go_to_score(score)
