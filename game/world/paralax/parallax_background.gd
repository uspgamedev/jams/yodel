extends ParallaxBackground

@export var dusk_tier: ProgressTier
@export var night_tier: ProgressTier

var play_transition := ''


func _ready():
	Shepherd.locate().state.begin_walk.connect(advance_stage)


func _on_tier_reached(tier: ProgressTier):
	if tier == dusk_tier:
		play_transition = 'day_to_dusk'
	elif tier == night_tier:
		play_transition = 'dusk_to_night'


func advance_stage():
	if play_transition != '':
		$AnimationPlayer.play(play_transition)
		play_transition = ''
