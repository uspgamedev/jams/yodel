extends Node

@export var dusk_scene: AnimationPlayer
@export var night_scene: AnimationPlayer

var dusk_background: ParallaxBackground
var night_background: ParallaxBackground
var background_stage = 0

func _ready():
	dusk_background = dusk_scene.parallax_background
	night_background = night_scene.parallax_background
	

func advance_stage():
	if background_stage == 0:
		dusk_scene.play("transition")
	elif background_stage == 1:
		night_scene.play("transition")
	background_stage += 1


func _on_dusk_scene_animation_finished(anim_name):
	dusk_scene.play_backwards(anim_name)
	dusk_scene.pause()


func _on_night_scene_animation_finished(anim_name):
	night_scene.play_backwards(anim_name)
	night_scene.pause()
