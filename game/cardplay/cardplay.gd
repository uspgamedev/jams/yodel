class_name CardPlay
extends Node

const GROUP := &'CARD_PLAY_GROUP'

@export_category('Gameplay Parameters')
@export var hand_max := 5
@export var played_card_max := 3
@export var starting_cards: Array[CardSpec]
@export var turn_max := 50
@export var progress_tiers: Array[ProgressTier]

@export_category('Node references')
@export var sender: SignalBusSender

var _draw_pile: Array[CardSpec] = []
var _discard_pile: Array[CardSpec] = []
var _hand: Array[CardSpec] = []
var _played: Array[CardSpec] = []
var _player_turn := false
var _score := 0
var _current_turn := 0
var _current_tier: ProgressTier = null
var _journey_ended := false


func _ready():
	add_to_group(GROUP)
	_try_update_tier.call_deferred()


static func locate(tree: SceneTree) -> CardPlay:
	return tree.get_nodes_in_group(GROUP).front()


func start_turn():
	if _player_turn or SignalBus.is_locked():
		return
	# Check end of journey
	if not _journey_ended and _current_turn == turn_max:
		sender.send_journey_ended(_score)
		_journey_ended = true
		return
	_try_update_tier()
	# Fill hand
	while _hand.size() < hand_max:
		if _draw_pile.is_empty():
			_draw_pile.append_array(_discard_pile)
			_draw_pile.shuffle()
			_discard_pile.clear()
			sender.send_piles_reshuffled(_draw_pile.size())
		var card := _draw_pile.pop_back() as CardSpec
		_hand.append(card)
		sender.send_card_drawn(card, _hand.size() - 1, _draw_pile.size())
	# Tick turn
	_player_turn = true
	_current_turn += 1


func play_card(position: int) -> bool:
	assert(position >= 0 and position < _hand.size())
	if not _player_turn or SignalBus.is_locked():
		return false
	if _played.size() < played_card_max:
		var card := _hand[position] as CardSpec
		if not card in _played:
			_hand.remove_at(position)
			_played.append(card)
			sender.send_card_played(card, position, _played.size())
			return true
	return false


func return_card(position: int) -> bool:
	if (position >= 0 and position < _played.size()):
		if SignalBus.is_locked():
			return false
		var card := _played[position] as CardSpec
		_played.remove_at(position)
		_hand.append(card) 
		sender.send_card_returned(card, position, _played.size())
		return true
	return false

func confirm_play():
	if not _player_turn or SignalBus.is_locked():
		return
	if _player_turn and _played.size() == played_card_max:
		var played := _played.duplicate()
		_played.clear()
		_discard_pile.append_array(played)
		_player_turn = false
		sender.send_play_confirmed(played, _discard_pile.size())


func earn_points(value: int):
	if not _journey_ended:
		var old_value := _score
		_score += value
		sender.send_score_changed(old_value, _score)


func _check_progress_tier() -> ProgressTier:
	var last_tier: ProgressTier = null
	for tier: ProgressTier in progress_tiers:
		if _score >= tier.threshold:
			last_tier = tier
	return last_tier

func _try_update_tier():
	# Check new tier reached
	var new_tier := _check_progress_tier()
	if new_tier != _current_tier:
		_current_tier = new_tier
		for card in _current_tier.introduced_cards:
			# FIXME: hard coded and coupled!
			_draw_pile.append(card)
			_draw_pile.append(card)
		_draw_pile.shuffle()
		sender.send_tier_reached(new_tier)

func get_score() -> int:
	return _score
	
func get_current_tier() -> ProgressTier:
	return _current_tier
