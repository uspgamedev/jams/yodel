extends Node

@onready var _streams: Array[AudioStreamPlayer] = [
	$Riff1 as AudioStreamPlayer,
	$Riff2 as AudioStreamPlayer,
	$Riff3 as AudioStreamPlayer
]


func _on_play_confirmed(cards: Array[CardSpec], _total_discarded):
	for card in get_tree().get_nodes_in_group("card_view"): 
		card.card_riff.stop()
		
	var riffs := {}
	for i in mini(cards.size(), _streams.size()):
		var card := cards[i]
		var stream_player := _streams[i]
		if riffs.get(card.musical_phrase) == null:
			stream_player.stream = card.musical_phrase
			stream_player.play()
			riffs[card.musical_phrase] = true
