extends Node
class_name Taste

@export var sprite: AnimatedSprite2D
@export var sheep_tastes: Array[CreatureSpec] = [] 
#@export var type := CreatureSpec.Type.SHEEP


var done := false

@export_dir var taste_folder_path: String = "null"
var taste: CreatureSpec

signal riffs(accepted: int, rejected: int)

func _ready():
	var tastes = sheep_tastes\
			.filter(func(t): return t.score_to_unlock <= CardPlay.locate(get_tree()).get_score())#\
			#.filter(func(t): return t.type == self.type)
	taste = tastes.pick_random()
	sprite.sprite_frames = taste.sprites
	$"../Label".text = taste.musical_taste + "-" + taste.musical_distaste + " = " + str(taste.score) 
	
	%SignalBusReceiver.play_confirmed.connect(check_song)

func check_song(cards: Array[CardSpec], _total_discarded):
	if done:
		return
	var card_dict = {}
	cards.filter(func(x): return x != null).map(func(card: CardSpec):
		card_dict[card.symbol] = true
	)
	
	
	var accept := 0
	var reject := 0
	
	for symbol in card_dict.keys():
		if taste.musical_taste.contains(symbol):
			accept += 1
		if taste.musical_distaste.contains(symbol):
			reject += 1
	
	if taste.type == CreatureSpec.Type.WOLF:
		accept = 1
	
	riffs.emit(accept, reject)
