# CHANGELOG

## 1.0.3

- Fix audio bus configuration
- Fix props not spawning in the respective stage
- Add timeline progress indicator
- Add right click plays card sounds
- Fix image artifacts

## 1.0.2

- Tweak sheep flock spatial distribution
- Fix props showing in weird places
- Fix right-side UI being dislocated when logs shows up
- Flash additions to sheep reaction log
- Add missing final score images
- Add confirm button when new cards introduced
- Remove from log duplicate sheep reactions in same turn

## 1.0.1

- Use number font in score scene

## 1.0.0

- First version submitted to Ludum Dare
